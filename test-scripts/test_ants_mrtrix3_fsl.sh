#!/bin/bash

# Check if software is installed.

echo "Check if ANTs, MRtrix3, and FSL are properly installed."

#ANTs
echo "Check ANTs"
which antsRegistrationSyN.sh > /dev/null
if [ $? -eq 0 ]; then
  ants_result="SUCCESS"
else
  ants_result="ERROR"
fi

#MRtrix3
echo "Check MRtrix3"
which mrconvert > /dev/null
if [ $? -eq 0 ]; then
  mrtrix3_result="SUCCESS"
else
  mrtrix3_result="ERROR"
fi

#FSL
echo "Check FSL"
which fsl > /dev/null
if [ $? -eq 0 ]; then
  fsl_result="SUCCESS"
else
  fsl_result="ERROR"
fi

echo ""
echo "Software:         Results"
echo "--------------------------"
echo "ANTs:              ${ants_result}"
echo "MRtrix3:           ${mrtrix3_result}"
echo "FSL:               ${fsl_result}"
echo "--------------------------"

exit

