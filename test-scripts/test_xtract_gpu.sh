#!/bin/bash
# script to test extract
# Test scripts were provided by Dr. Tetsuo Koyama
# 06 May 2023 K. Nemoto

archive=DTI_pipeline_L4N-GPU_20230417.tar.gz

cd $HOME/Downloads

if [[ ! -e ${archive} ]];then
  echo "Download a test archive"
  [[ -d ${archive%.tar.gz} ]] && rm -rf ${archive%.tar.gz}
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${archive}
  tar xvzf ${archive}
else
  echo "Found the test archive"
  [[ -d ${archive%.tar.gz} ]] && rm -rf ${archive%.tar.gz}
  tar xvzf ${archive}
fi

cd ${archive%.tar.gz}

# Run scripts
time ./automator.sh


