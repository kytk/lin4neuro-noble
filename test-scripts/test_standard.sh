#!/bin/bash

# Check if software is installed.

echo "Check if 3D Slicer, ANTs, DSI Studio, FSL, MRtrix3, and SPM12 are properly installed."

#3D Slicer
echo "Check 3D Slicer"
which Slicer > /dev/null
if [ $? -eq 0 ]; then
  slicer_result="SUCCESS"
else
  slicer_result="ERROR"
fi

#ANTs
echo "Check ANTs"
which antsRegistrationSyN.sh > /dev/null
if [ $? -eq 0 ]; then
  ants_result="SUCCESS"
else
  ants_result="ERROR"
fi

#DSI Studio
echo "Check DSI Studio"
which /usr/local/dsi-studio/dsi_studio > /dev/null
if [ $? -eq 0 ]; then
  dsistudio_result="SUCCESS"
else
  dsistudio_result="ERROR"
fi

#FSL
echo "Check FSL"
which fsl > /dev/null
if [ $? -eq 0 ]; then
  fsl_result="SUCCESS"
else
  fsl_result="ERROR"
fi

#ITK-SNAP
echo "Check ITK-SNAP"
which itksnap > /dev/null
if [ $? -eq 0 ]; then
  itksnap_result="SUCCESS"
else
  itksnap_result="ERROR"
fi

#MRtrix3
echo "Check MRtrix3"
which mrconvert > /dev/null
if [ $? -eq 0 ]; then
  mrtrix3_result="SUCCESS"
else
  mrtrix3_result="ERROR"
fi

#SPM12
echo "Check SPM12"
which /usr/local/spm12_standalone/run_spm12.sh > /dev/null
if [ $? -eq 0 ]; then
  spm12_result="SUCCESS"
else
  spm12_result="ERROR"
fi

echo ""
echo "Software:         Results"
echo "--------------------------"
echo "3D Slicer:         ${slicer_result}"
echo "ANTs:              ${ants_result}"
echo "DSI Studio:        ${dsistudio_result}"
echo "FSL:               ${fsl_result}"
echo "ITK-SNAP:          ${itksnap_result}"
echo "MRtrix3:           ${mrtrix3_result}"
echo "SPM12:             ${spm12_result}"
echo "--------------------------"

exit

