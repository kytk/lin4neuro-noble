#!/bin/bash
# script to test eddy_cuda
# 08 Dec 2022 K. Nemoto

cd $HOME/Downloads

if [[ ! -e eddy_cuda_test.zip ]];then
  echo "Download a test archive"
  [[ -d eddy_cuda_test ]] && rm -rf eddy_cuda_test
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/eddy_cuda_test.zip
  unzip eddy_cuda_test.zip
else
  echo "Found the test archive"
  [[ -d eddy_cuda_test ]] && rm -rf eddy_cuda_test
  unzip eddy_cuda_test.zip
fi

cd eddy_cuda_test

# Run eddy_cuda
echo "Begin eddy_cuda"
time eddy_cuda10.2 --imain=dwidata \
     --mask=hifi_nodif_brain_mask \
     --index=index.txt \
     --acqp=acqparams.txt \
     --bvecs=bvecs \
     --bvals=bvals \
     --fwhm=0 \
     --topup=topup_AP_PA_b0 \
     --flm=quadratic \
     --out=eddy_unwarped_images \
     --data_is_shelled

# Check the output exists
if [[ -e eddy_unwarped_images.nii.gz ]]; then
  echo "eddy_cuda was done successfully."
  exit 0
else
  echo "ERROR: it seems eddy_cuda exited with some errors."
  exit 1
fi
