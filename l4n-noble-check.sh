#!/bin/bash

# Check if software is installed.

echo "Check if neuroimaging software is properly installed."

#3D Slicer
#echo "Run Slicer"
#Slicer &
#wait

#Aliza
#echo "Run Aliza"
#aliza &
#wait

#c3d
echo "Run c3d"
c3d -h > /dev/null
if [ $? -eq 0 ]; then
  c3d_result="SUCCESS"
else
  c3d_result="ERROR"
fi

#DSI Studio
#echo "Run DSI-Studio"
#/usr/local/dsistudio/dsi_studio &
#wait

#ITK-SNAP
#echo "Run itksnap"
#itksnap &
#wait

#Mango
echo "Run Mango"
mango &
if [ $? -eq 0 ]; then
  mango_result="SUCCESS"
else
  mango_result="ERROR"
fi
sleep 3
mango_job=$(ps aux | grep Mango.jar | awk '{ print $2 }' | sed -n 1p)
kill ${mango_job}


#MRIcroGL
echo "Run MRIcroGL"
MRIcroGL /usr/local/MRIcroGL/Resources/standard/spm152.nii.gz &
if [ $? -eq 0 ]; then
  mricrogl_result="SUCCESS"
else
  mricrogl_result="ERROR"
fi
mricrogl_job=$(ps aux | grep MRIcroGL | awk '{ print $2 }' | sed -n 1p)
kill ${mricrogl_job}


#dcm2niix
echo "Run dcm2niix"
dcm2niix > /dev/null
if [ $? -eq 0 ]; then
  dcm2niix_result="SUCCESS"
else
  dcm2niix_result="ERROR"
fi


#dcmtk
echo "Run dcmdump"
dcmdump > /dev/null
if [ $? -eq 0 ]; then
  dcmtk_result="SUCCESS"
else
  dcmtk_result="ERROR"
fi



#MRIcron
echo "Run MRIcroN"
MRIcron /usr/local/mricron/Resources/templates/ch2.nii.gz &
if [ $? -eq 0 ]; then
  mricron_result="SUCCESS"
else
  mricron_result="ERROR"
fi
mricron_job=$(ps aux | grep MRIcron | awk '{ print $2 }' | sed -n 1p)
kill ${mricron_job}



#ROBEX
echo "Run ROBEX"
runROBEX.sh > /dev/null
if [ $? -eq 0 ]; then
  robex_result="SUCCESS"
else
  robex_result="ERROR"
fi

#Surf-Ice
echo "Run Surf-Ice"
surfice &
if [ $? -eq 0 ]; then
  surfice_result="SUCCESS"
else
  surfice_result="ERROR"
fi
surfice_job=$(ps aux | grep surfice | awk '{ print $2 }' | sed -n 1p)
kill ${surfice_job}


#Talairach daemon
echo "Run Talairach_daemon"
java -jar /usr/local/tdaemon/talairach.jar &
if [ $? -eq 0 ]; then
  td_result="SUCCESS"
else
  td_result="ERROR"
fi
td_job=$(ps aux | grep talairach.jar | awk '{ print $2 }' | sed -n 1p)
kill ${td_job}


#Virtual MRI
echo "Run Virtual_MRI"
java -jar /usr/local/vmri/vmri.jar & > /dev/null
if [ $? -eq 0 ]; then
  vmri_result="SUCCESS"
else
  vmri_result="ERROR"
fi
vmri_job=$(ps aux | grep vmri.jar | awk '{ print $2 }' | sed -n 1p)
kill ${vmri_job}

echo ""
echo "Software:         Results"
echo "--------------------------"
echo "c3d:              ${c3d_result}"
echo "Mango:            ${mango_result}"
echo "MRIcroGL:         ${mricrogl_result}"
echo "dcm2niix:         ${dcm2niix_result}"
echo "DCMTK:            ${dcmtk_result}"
echo "MRIcron:          ${mricron_result}"
echo "ROBEX:            ${robex_result}"
echo "Surf-Ice:         ${surfice_result}"
echo "Talairach_daemon: ${td_result}"
echo "Virtual_MRI:      ${vmri_result}"
echo "--------------------------"

exit

