#!/bin/bash

# Lin4Neuro building script for Ubuntu 24.04 (Noble)
# Part2: Install Neuroimaging software packages
# This script installs neuroimaging software packages such as
# AlizaMS, DCMTK, Heudiconv, Octave, R, c3d, Mango, MRIcroGL, 
# MRIcron, ROBEX, Surf-Ice, Talairach daemon, and Virtual MRI.
# Installers for AFNI, FSL, SPM standalone and other software are also provided.
# Pre-requisite: You need to setup Lin4Neuro beforehand.
# 25 Apr 2024 K. Nemoto

##### STEP 0. PREPARATION ######################################################
# Log
log=$(date +%Y%m%d%H%M%S)-vm2.log
exec &> >(tee -a "$log")

# Working directory
wd=$PWD # should be lin4neuro-noble

# .bash_aliases
[ -e ~/.bash_aliases ] || touch ~/.bash_aliases

# Settings for Japanese
if [ $LANG == "ja_JP.UTF-8" ]; then
  LANG=C xdg-user-dirs-update --force
  cd $HOME
  if [ -d ダウンロード ]; then
    rmdir ダウンロード テンプレート デスクトップ ドキュメント ビデオ \
           ピクチャ ミュージック 公開
  fi
  im-config -n fcitx
fi

# alias for xdg-open
grep xdg-open ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#alias for xdg-open' >> ~/.bash_aliases
    echo alias open='xdg-open &> /dev/null' >> ~/.bash_aliases
fi

# copy desktop and icons
cp $wd/lin4neuro-parts/local/share/applications/*.desktop \
   ~/.local/share/applications
cp -r $wd/lin4neuro-parts/local/share/icons ~/.local/share/


##### STEP 1. INSTALL NEUROIMAGING SOFTWARE PACKAGES USING APT #################

timezone=$(ls -l /etc/localtime | awk -F/ '{ print $NF }')
if [[ $timezone == "Tokyo" ]]; then
  #Setup Neurodebian repository using repo in Japan
  cat << EOS > neurodebian.sources.list
  deb [arch=amd64 signed-by=/usr/share/keyrings/neurodebian.gpg] http://neuroimaging.sakura.ne.jp/neurodebian data main contrib non-free
  deb [arch=amd64 signed-by=/usr/share/keyrings/neurodebian.gpg] http://neuroimaging.sakura.ne.jp/neurodebian $(lsb_release -cs) main contrib non-free
EOS
else
  #Setup Neurodebian repository using in repo in us-nh
  cat << EOS > neurodebian.sources.list
  deb [arch=amd64 signed-by=/usr/share/keyrings/neurodebian.gpg] http://neuro.debian.net/debian data main contrib non-free
  deb [arch=amd64 signed-by=/usr/share/keyrings/neurodebian.gpg] http://neuro.debian.net/debian $(lsb_release -cs) main contrib non-free
EOS
fi
sudo mv neurodebian.sources.list /etc/apt/sources.list.d/

# Signature for Neurodebian
wget -O- http://neuro.debian.net/_static/neuro.debian.net.asc |\
  sudo gpg --dearmor --yes --output /usr/share/keyrings/neurodebian.gpg
# uncomment the following line if the above links cannot be reached.
#sudo gpg --dearmor neurodebian.net.asc --yes --output /usr/share/keyrings/neurodebian.gpg
sudo apt-get update


# Curl (reassure to install)
sudo apt-get install -y curl


echo "Install neuroimaging-related software packages"

#DCMTK
echo "Install DCMTK"
sudo apt-get install -y dcmtk


#Octave
echo "Install Octave"
sudo apt-get install -y octave
sudo pip3 install octave_kernel


#R
echo "Install R"
sudo apt-get install -y r-base


#R (cloud.r-project.org; for version 4.1)
#sudo apt-key adv --keyserver keyserver.ubuntu.com \
#     --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
#
#echo "Install R using cran.rstudio.com repository"
#
#grep rstudio /etc/apt/sources.list > /dev/null
#if [ $? -eq 1 ]; then
#  sudo add-apt-repository \
#  'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran40/'
#fi
#sudo apt-get -y update

echo -e "Install python libraries \n"
#Installation of python libraries for machine learning
pip3 install --user jupyter notebook numpy scipy opencv-python python-dateutil
pip3 install --user cmake matplotlib pyyaml h5py pydot-ng pillow 
pip3 install --user scikit-learn pandas
pip3 install --user pydicom heudiconv dcm2bids gdcm


##### STEP 2. INSTALL NEUROIMAGING SOFTWARE PACKAGES MANUALLY  #################

# AlizaMS
echo "Install Aliza MS"

alizadeb='alizams_1.9.9+git0.e1a3323-1+1.1_amd64.deb'

cd $HOME/Downloads

if [ ! -e ${alizadeb} ]; then
  curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${alizadeb}
fi

sudo apt install ./${alizadeb}

if [ ! -e ~/.local/share/aaplications/alizams.desktop ]; then
  find $HOME -name 'alizams.desktop' 2>/dev/null -exec cp {} ~/.local/share/applications/ \;
fi

sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/alizams.desktop


# c3d
echo "Install c3d"
cd "$HOME"/Downloads

if [ ! -e 'c3d-1.0.0-Linux-x86_64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/c3d-1.0.0-Linux-x86_64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/c3d-1.0.0-Linux-x86_64.tar.gz
sudo mv c3d-1.0.0-Linux-x86_64 c3d

grep c3d ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# c3d' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/c3d/bin' >> ~/.bash_aliases
    echo 'source /usr/local/c3d/share/bashcomp.sh' >> ~/.bash_aliases
fi


#Mango
echo "Install Mango"
cd $HOME/Downloads

if [ ! -e 'mango_unix.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mango_unix.zip
fi

cd /usr/local
sudo unzip ~/Downloads/mango_unix.zip

grep Mango ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# Mango' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Mango' >> ~/.bash_aliases
fi


#MRIcroGL
echo "Install MRIcroGL"
sudo apt-get -y install appmenu-gtk2-module

cd $HOME/Downloads

curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip

cd /usr/local
sudo unzip ~/Downloads/MRIcroGL_linux.zip

grep MRIcroGL ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=/usr/local/MRIcroGL:$PATH' >> ~/.bash_aliases
    echo 'export PATH=/usr/local/MRIcroGL/Resources:$PATH' >> ~/.bash_aliases
fi

# dcm2niix
echo "Install dcm2niix"
ver=v1.0.20240202

cd $HOME/Downloads

rm dcm2niix_lnx.zip
curl -OL https://github.com/rordenlab/dcm2niix/releases/download/${ver}/dcm2niix_lnx.zip

[[ -d /usr/local/dcm2niix ]] || sudo mkdir /usr/local/dcm2niix
sudo unzip dcm2niix_lnx.zip -d /usr/local/dcm2niix

grep '# dcm2niix' ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> ~/.bash_aliases
  echo '# dcm2niix' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/dcm2niix:$PATH' >> ~/.bash_aliases
fi


# MRIcron
echo "Install MRIcron"
cd $HOME/Downloads

if [ ! -e 'MRIcron_linux.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcron_linux.zip
fi

cd /usr/local
sudo unzip ~/Downloads/MRIcron_linux.zip

sudo find /usr/local/mricron -name 'dcm2niix' -exec rm {} \;
 # Delete dcm2niix because this version is old

sudo find /usr/local/mricron -name '*.bat' -exec rm {} \;
 # Delete batch files since they are not available anymore

sudo find /usr/local/mricron -type d -exec chmod 755 {} \;
sudo find /usr/local/mricron/Resources -type f -exec chmod 644 {} \;
sudo chmod 755 /usr/local/mricron/Resources/pigz_mricron

grep mricron ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRIcron' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/mricron' >> ~/.bash_aliases
fi


# ROBEX
echo "Install ROBEX"
cd $HOME/Downloads

if [ ! -e 'ROBEXv12.linux64.tar.gz' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/ROBEXv12.linux64.tar.gz
fi

cd /usr/local
sudo tar xvzf ~/Downloads/ROBEXv12.linux64.tar.gz
sudo chmod 755 ROBEX
cd ROBEX
sudo find -type f -exec chmod 644 {} \;
sudo chmod 755 ROBEX runROBEX.sh dat ref_vols

grep ROBEX ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# ROBEX' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/ROBEX' >> ~/.bash_aliases
fi


# Surf-Ice
echo "Install Surf-Ice"
cd $HOME/Downloads
curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/surfice_linux.zip

cd /usr/local
sudo unzip ~/Downloads/surfice_linux.zip

cd Surf_Ice
sudo find . -type d -exec chmod 755 {} \;
sudo find . -type f -exec chmod 644 {} \;
sudo chmod 755 surfice*
sudo chmod 644 surfice_Linux_Installation.txt

grep Surf_Ice ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# Surf_Ice' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Surf_Ice' >> ~/.bash_aliases
fi


# Talairach daemon
echo "Install Talairach daemon"
sudo cp -r ${wd}/lin4neuro-parts/tdaemon /usr/local
grep Talairach ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> ~/.bash_aliases
  echo '# Talairach daemon' >> ~/.bash_aliases
  echo "alias tdaemon='java -jar /usr/local/tdaemon/talairach.jar'" >> ~/.bash_aliases
fi


# VirtualMRI
echo "Install Virtual MRI"
cd $HOME/Downloads

if [ ! -e 'vmri.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/vmri.zip
fi

cd /usr/local
sudo unzip ~/Downloads/vmri.zip


# Tutorial
echo "Install tutorial by Chris Rorden"
cd $HOME/Downloads

if [ ! -e 'tutorial.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/tutorial.zip
fi

cd $HOME
unzip ~/Downloads/tutorial.zip
find tutorial -type f -exec chmod 644 {} \;
find tutorial -type d -exec chmod 755 {} \;


##### STEP 3. POST-INSTALLATION TUNING #########################################

# Remove unnecessary files
sudo find / -name '__MACOSX' -exec rm -rf {} \;
sudo find / -name '.DS_Store' -exec rm -rf {} \;
sudo find / -name '._*' -exec rm {} \;
sudo find / -name 'Thumbs.db' -exec rm {} \;


## Symbolic link to the installer
#cat << EOS >> ~/.profile
#
## symbolic link to the installer
#if [ ! -L ~/Desktop/installer ]; then
#   ln -fs ~/git/lin4neuro-noble/installer ~/Desktop
#fi
#
#EOS

# copy .bash_aliases and .profile to /etc/skel
sudo cp ~/.bash_aliases ~/.profile /etc/skel


# Change /bin/sh from dash to bash
echo "dash dash/sh boolean false" | sudo debconf-set-selections
sudo dpkg-reconfigure --frontend=noninteractive dash


echo "Finished!"

exit

