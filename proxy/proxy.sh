# Proxy setting
# Please put this file under /etc/profile.d/

export http_proxy="http://xx.xx.xx.xx:xxxx/"
export https_proxy="http://xx.xx.xx.xx:xxxx/"
export ftp_proxy="http://xx.xx.xx.xx:xxxx/"
export no_proxy="127.0.0.1,localhost"

# For curl
export HTTP_PROXY="http://xx.xx.xx.xx:xxxx"
export HTTPS_PROXY="http://xx.xx.xx.xx:xxxx"
export FTP_PROXY="http://xx.xx.xx.xx:xxxx"
export NO_PROXY="127.0.0.1,localhost"

