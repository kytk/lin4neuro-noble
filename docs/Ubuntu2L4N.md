# Ubuntu から Lin4Neuro への変換

## Ubuntu のインストール
Ubuntu desktop を使って「最小インストール」を行います。

- [Ubuntu Desktop 22.04 LTS](https://jp.ubuntu.com/download/thank-you?version=22.04.4&architecture=amd64&platform=desktop) から、イメージをダウンロードします

- ダウンロードしたisoファイルをUSBメモリに展開します
    - Windowsで作成するならば、[Rufus](https://rufus.ie/ja/) などを使うといいかもしれません

- USBメモリからPCを起動します
    - インストールは「最小インストール」を選択します
      ![最小インストール](img/u2l_01.png)

- インストール後、再起動します

- 最初に、画面がロックされないように設定します
    - 「設定」を立ち上げます
    - 「プライバシー」→ 「画面」を選択します

      ![display_settings](img/display_settings.png)

        - 「ブランクスクリーンの遅延」を「しない」にします
        - 「自動画面ロック」を off にします

- ターミナルを起動します

- git をインストールします

    ```
    sudo apt install -y git
    ```

- lin4neuro-jammy のリポジトリをダウンロードします

    ```
    mkdir ~/git
    cd ~/git
    git clone https://gitlab.com/kytk/lin4neuro-jammy.git
    ```

- l4n-jammy-1.sh を実行します

    ```
    cd lin4neuro-jammy
    ./l4n-jammy-1.sh
    ```

    - Which language do you want to build? は日本語を選択します

    - Configuring lightdm は、lightdm を選択します
      ![lightdm](img/u2l_02.png)

- 終わると、"Finished! Please run l4n-jammy-2.sh to install neuroimaging packages after reboot." と表示されますので、再起動します


- これで、脳画像解析ソフトを入れる前までの準備は終了です

## 基本的な脳画像解析ソフトのインストール
- 再度ターミナルを起動し、l4n-jammy-2.sh を実行します

    ```
    cd ~/git/lin4neuro-jammy
    ./l4n-jammy-2.sh
    ```

    - これによって、基本的な脳画像解析ソフトが自動でインストールされます

- 最後に脳画像解析ソフトが正しくインストールされたかチェックします

- 改めてターミナルを起動します

- 以下のコマンドを実行します

    ```
    cd ~/git/lin4neuro-jammy
    ./l4n-jammy-check.sh
    ```
    
- すべて正しく入れば以下の表示になります。


    ```
    Check if neuroimaging software is properly installed.
    Run c3d
    Run Mango
    Run MRIcroGL
    Run dcm2niix
    Run dcmdump
    Run MRIcroN
    Run ROBEX
    Run Surf-Ice
    Run Talairach_daemon
    Run Virtual_MRI
    
    Software:         Results
    --------------------------
    c3d:              SUCCESS
    Mango:            SUCCESS
    MRIcroGL:         SUCCESS
    dcm2niix:         SUCCESS
    DCMTK:            SUCCESS
    MRIcron:          SUCCESS
    ROBEX:            SUCCESS
    Surf-Ice:         SUCCESS
    Talairach_daemon: SUCCESS
    Virtual_MRI:      SUCCESS
    --------------------------
    ```

- これで、基本的なソフトはインストールされました。それ以外のソフトは以下で行います

## その他の画像解析ソフトのインストール

- 以下のコマンドでインストールできます

    ```
    cd ~/git/lin4neuro-jammy/installer-scripts
    ./install_standard_applications.sh
    ```

- これによって、以下のソフトがインストールされます

    - 3D Slicer
    - ANTs
    - DSI Studio
    - FSL
    - MRtrix3
    - SPM12


### ANTs, MRtrix3, FSL の検証

- 以下のコマンドで確認できます

    ```
    cd ~/git/lin4neuro-jammy/test-scripts/
    ./test_ants_mrtrix3_fsl.sh
    ```

- 以下のようになるはずです

    ```
    Check if ANTs, MRtrix3, and FSL are properly installed.
    Check ANTs
    Check MRtrix3
    Check FSL
    
    Software:         Results
    --------------------------
    ANTs:              SUCCESS
    MRtrix3:           SUCCESS
    FSL:               SUCCESS
    --------------------------
    ```


## GPUを使う場合の設定(ハードウェアにGPUを搭載する場合のみ以下を参照してください)

### CUDA のインストール

- FSLは eddyに cuda を使用することができます。以下のコマンドでインストールできます。FSL 6.0.6.2からcudaのバージョン指定は不要となりました

    ```
    cd ~/git/lin4neuro-jammy/installer-scripts
    ./cuda_installer.sh
    ```

- 以下の検証を行うために、一度再起動してください

### eddy_cuda の検証

- インストールしたCUDAが脳画像解析ソフトFSLからきちんと呼び出されるか検証します
- こちらもスクリプトを準備してあります。以下を実行してください

    ```
    cd ~/git/lin4neuro-jammy/test-scripts
    ./test_eddy_cuda.sh
    ```

- このスクリプトは、$HOME/Downloads に eddy_cuda_test.zip をダウンロードし、そこで展開し、eddy_cuda_test ディレクトリの中で画像解析を走らせます。正しく走れば、eddy_unwarped_images.nii.gz をはじめとした、eddy_unwarped_images からはじまる様々なファイルが生成されます。かかる時間はおおよそ10分程度です

- "eddy_cuda was done successfully." と表示されたら解析が正しく行われたことになります


### bedpostx_gpu の検証

- 以下でテストできます

    ```
    cd ~/git/lin4neuro-jammy/test-scripts
    ./test_bedpostx_gpu.sh
    ```

- 以下のようになれば成功しています。数分で終わります

    ```
    ---------------------------------------------
    ------------ BedpostX GPU Version -----------
    ---------------------------------------------
    subjectdir is /home/kiyotaka/git/gpu_test/btest/bedpost
    Making bedpostx directory structure
    Copying files to bedpost directory
    Pre-processing stage
    Queuing parallel processing stage
    
    ----- Bedpostx Monitor -----
    Queuing post processing stage
    1 parts processed out of 4
    2 parts processed out of 4
    3 parts processed out of 4
    4 parts processed out of 4
    All parts processed
    
    real	1m25.898s
    user	0m24.227s
    sys	0m1.134s
    ```


- これでインストールおよび検証は終了です

