# How to add additional hard disks

## Check devices

    ```
    sudo fdisk -l
    ```

## make a new partition for the device

    ```
    sudo gdisk /dev/sda # if you want to make new partition for /dev/sda
    ```

    - Command: n # add a new partition
    - Command: p # print the partition table
    - Command: w # write table to disk and exit

## format as ext4

    ```
    sudo mkfs.ext4 /dev/sda
    ```


## find UUID for hard disks

    ```
    sudo blkid | grep dev\/sd
    ```



## make mount points

    ```
    sudo mkdir /mnt/data1
    sudo chown -R $USER:$USER /mnt/data1
    ```

## Add to /etc/fstab

    ```
    UUID=<uuid> /mnt/data1	ext4	defaults	0	0
    ```

## mount hard disks

    ```
    sudo mount -a
    ```


