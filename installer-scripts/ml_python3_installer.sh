#!/bin/bash
#A script to install python libraries for machine learning and Spyder3

echo "This script installs various Python3 libraries for machine learning."
echo "Do you want to continue? (yes/no)"

read answer

case $answer in
        [Yy]*)
		echo -e "Install libraries \n"
		#Installation of python libraries for machine learning
                pip3 install --user jupyter notebook numpy scipy opencv-python python-dateutil
		pip3 install --user cmake matplotlib pyyaml h5py pydot-ng pillow 
		pip3 install --user tensorflow
		
		#Installation of Spyder3
		#pip3 install --user PyQtWebEngine spyder

                #Paths settings
                grep CMake ~/.bash_aliases > /dev/null
                if [ $? -eq 1 ]; then
                  echo '' >> ~/.bash_aliases
                  echo '# CMake' >> ~/.bash_aliases
                  echo 'export PATH=$PATH:~/.local/bin' >> ~/.bash_aliases
                fi

                break
                ;;
        [Nn]*)
                echo -e "Run this script later.\n"
                exit
                ;;
        *)
                echo -e "Type yes or no.\n"     
                ;;
esac


