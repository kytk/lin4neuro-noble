#!/bin/bash
#CONN22a standalone installer

#Check MCR is installed
if [ ! -d /usr/local/MATLAB/MCR/R2022b ]; then
  echo "Matlab Compiler Runtime needs to be installed first!"
  ~/git/lin4neuro-jammy/installer-scripts/mcr_v913_installer.sh
fi

#Download CONN22a standalone
echo "Download CONN22a standalone"
cd $HOME/Downloads

if [ ! -e 'conn22a_glnxa64.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/conn22a_glnxa64.zip
fi

cd /usr/local
sudo mkdir conn22a_standalone
cd conn22a_standalone
sudo unzip ~/Downloads/conn22a_glnxa64.zip
sudo chown -R $USER:$USER /usr/local/conn22a_standalone

#Desktop entry
cat << EOS > ~/.local/share/applications/conn22a.desktop
[Desktop Entry]
Encoding=UTF-8
Name=CONN 22a
Exec=bash -c '/usr/local/conn22a_standalone/run_conn.sh /usr/local/MATLAB/MCR/R2022b'
Icon=conn.png
Type=Application
Terminal=true
Categories=Neuroimaging;
NoDisplay=false
EOS

#alias
grep conn22a ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#conn22a standalone' >> ~/.bash_aliases
    echo "alias conn='/usr/local/conn22a_standalone/run_conn.sh /usr/local/MATLAB/MCR/R2022b'" >> ~/.bash_aliases
fi

echo "Finished! Run CONN from menu -> Neuroimaging -> CONN"
sleep 5 
exit

