#!/bin/bash

itksnapver="itksnap-4.0.1-20230320-Linux-gcc64.tar.gz"

echo "Install ITK-SNAP"
cd $HOME/Downloads

if [ ! -e ${itksnapver} ]; then
  curl -O  http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${itksnapver}
fi

cd /usr/local
sudo tar xvzf ~/Downloads/${itksnapver}
sudo mv ${itksnapver%.tar.gz} itksnap
grep itksnap ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#ITK-SNAP' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/itksnap/bin' >> ~/.bash_aliases
fi

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/itksnap.desktop

echo "Finished!"
sleep 5
exit
