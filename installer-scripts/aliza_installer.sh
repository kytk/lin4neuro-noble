#!/bin/bash

deb='aliza_2.3.12.8_amd64.deb'

echo "Install Aliza"
cd $HOME/Downloads

if [ ! -e ${deb} ]; then
  curl -O -C - http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${deb}
fi

sudo apt install ./${deb}

if [ ! -e ~/.local/share/aaplications/aliza.desktop ]; then
  find $HOME -name 'aliza.desktop' 2>/dev/null -exec cp {} ~/.local/share/applications/ \;
fi

#make icon show in the neuroimaging directory
sed -i 's/NoDisplay=true/NoDisplay=false/' ~/.local/share/applications/aliza.desktop

echo "Finished!"
sleep 5
exit
