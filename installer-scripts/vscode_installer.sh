#!/bin/bash
# Installer script for VS code
# 06 May 2023 K. Nemoto

# For debugging
set -x

package=vscode
pubkey=${package}.asc
keylink='https://packages.microsoft.com/keys/microsoft.asc'
aptlink='https://packages.microsoft.com/repos/vscode stable main'
aptlist=${package}.list

cd $HOME/Downloads
[[ -f ${pubkey} ]] && rm ${pubkey}
curl ${keylink} -o ${pubkey}

sudo gpg --output /usr/share/keyrings/${package}.gpg --dearmor ${package}.asc 

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/${package}.gpg] ${aptlink}" | sudo tee /etc/apt/sources.list.d/${aptlist}

sudo apt-get update
sudo apt-get install -y code

exit

