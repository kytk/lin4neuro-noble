#!/bin/bash

# Install 3D Slicer, ANTs, DSI Studio, FSL, ITK-SNAP, MRtrix3, and SPM12

./3dslicer_installer.sh
./ants_installer_zip.sh
./dsistudio_installer_jammy.sh
./fsl_installer_archive.sh
./itksnap_installer.sh
./mrtrix3_installer_zip.sh
./spm12_installer_R2022b.sh

