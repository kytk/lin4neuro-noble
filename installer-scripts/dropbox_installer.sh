#!/bin/bash
# Installer script for Dropbox
# 06 May 2023 K. Nemoto

package=dropbox
keylink='https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x1c61a2656fb57b7e4de0f4c1fc918b335044912e'
link='http://linux.dropbox.com/ubuntu jammy main'
list=dropbox.list

cd $HOME/Downloads
[[ -f dropbox.asc ]] && rm dropbox.asc
curl ${keylink} -o ${package}.asc

sudo gpg --output /usr/share/keyrings/${package}.gpg --dearmor ${package}.asc 

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/${package}.gpg] ${link}" | sudo tee /etc/apt/sources.list.d/${package}.list

sudo apt-get update
sudo apt-get install -y ${package}

# Workaround to synchronize all files
grep fs.inotify.max_user_watches=100000  /etc/sysctl.conf > /dev/null
if [ $? -eq 1 ]; then
  echo fs.inotify.max_user_watches=100000 | \
  sudo tee -a /etc/sysctl.conf; sudo sysctl -p
fi

exit

