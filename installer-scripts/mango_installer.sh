#!/bin/bash

echo "Install Mango"
cd $HOME/Downloads

if [ ! -e 'mango_unix.zip' ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/mango_unix.zip
fi

cd /usr/local
sudo unzip ~/Downloads/mango_unix.zip

grep Mango ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# Mango' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/Mango' >> ~/.bash_aliases
fi

[ -d ~/bin ] || mkdir ~/bin
cat << 'EOS' > ~/bin/mango
#!/bin/bash
/usr/local/Mango/jre7/bin/java -Djava.awt.headless=true -Xms64M -Xmx512M -XX:MaxDirectMemorySize=1536M -cp "/usr/local/Mango/Mango.jar" edu.uthscsa.ric.mango.MangoClient "$@"
EOS

