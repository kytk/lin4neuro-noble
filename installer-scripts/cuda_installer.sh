#!/bin/bash

# This is based on the link below from NVIDIA
#https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#ubuntu

# Fore debugging
set -x

# Delete previous public key
sudo apt-key del 7fa2af80

# Install the new cuda-keyring package
cd ~/Downloads
[[ -e cuda-keyring_1.1-1_all.deb ]] && rm cuda-keyring_1.1-1_all.deb
if [[ ! -e /etc/apt/sources.list.d/cuda-ubuntu2204-x86_64.list ]]; then
  wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.1-1_all.deb
  sudo dpkg -i cuda-keyring_1.1-1_all.deb 
fi

# Install CUDA
# Select CUDA 11.8 to ensure it works without problems for FSL
sudo apt-get update
sudo apt-get -y install --no-install-recommends cuda cuda-11-8

# hold cuda-11-8 so that it won't be removed updating cuda
sudo apt-mark hold cuda-11-8

# Update-alternatives
sudo update-alternatives --install /usr/local/cuda cuda /usr/local/cuda-11.8 118
sudo update-alternatives --set cuda /usr/local/cuda-11.8

# Add PATH
grep CUDA ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
  echo '' >> ~/.bash_aliases
  echo '# CUDA' >> ~/.bash_aliases
  echo 'export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}' >> ~/.bash_aliases
fi

exit

