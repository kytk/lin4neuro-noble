#!/bin/bash
#A script to install python libraries for machine learning and Spyder3

echo "This script installs various Python3 libraries."

echo -e "Install libraries \n"
#Installation of python libraries for machine learning
pip3 install --user jupyter notebook numpy scipy opencv-python python-dateutil
pip3 install --user cmake matplotlib pyyaml h5py pydot-ng pillow 

#Installation of tensorflow
#pip3 install --user tensorflow

#Installation of Spyder3
#pip3 install --user PyQtWebEngine spyder
