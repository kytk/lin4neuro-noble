#!/bin/bash

# Script to install MRIcroGL
# 14 May 2023 K. Nemoto

# version
ver=v1.2.20220720

# make sure to install appmenu-gtk2-module
sudo apt-get install -y appmenu-gtk2-module

echo "Delete old version if it exists"
if [ -d /usr/local/MRIcroGL ]; then
  sudo rm -rf /usr/local/MRIcroGL
fi

echo "Install MRIcroGL"
cd $HOME/Downloads
if [ -e MRIcroGL_linux.zip ]; then
  rm MRIcroGL_linux.zip
fi

#curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/MRIcroGL_linux.zip

curl -L -O -C - https://github.com/rordenlab/MRIcroGL/releases/download/${ver}/MRIcroGL_linux.zip

cd /usr/local
sudo unzip ~/Downloads/MRIcroGL_linux.zip

grep MRIcroGL ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '# MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/MRIcroGL/Resources' >> ~/.bash_aliases
fi

