#!/bin/bash
#SPM12 standalone installer

ubuntuver='jammy'
mcrver='v99'

#Check MCR is installed
if [[ ! -d /usr/local/MATLAB/MCR/${mcrver} ]]; then
  echo "Matlab Compiler Runtime needs to be installed first!"
  ~/git/lin4neuro-jammy/installer-scripts/mcr_${mcrver}_installer.sh
fi

#Download SPM12 standalone
echo "Download SPM12 standalone"
cd $HOME/Downloads

if [ ! -e "spm12_standalone_${ubuntuver}_${mcrver}.zip" ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/spm12_standalone_${ubuntuver}_mcr${mcrver}.zip
fi

cd /usr/local
sudo unzip ~/Downloads/spm12_standalone_${ubuntuver}_mcr${mcrver}.zip
cd spm12_standalone
sudo chmod 755 run_spm12.sh spm12

#Desktop entry
cat << EOS > ~/.local/share/applications/spm12.desktop
[Desktop Entry]
Encoding=UTF-8
Name=SPM12
Exec=bash -c '/usr/local/spm12_standalone/run_spm12.sh /usr/local/MATLAB/MCR/v99'
Icon=spm12.png
Type=Application
Terminal=true
Categories=Neuroimaging;
NoDisplay=false
EOS

#alias 
grep SPM12 ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#SPM12 standalone' >> ~/.bash_aliases
    echo "alias spm='/usr/local/spm12_standalone/run_spm12.sh /usr/local/MATLAB/MCR/v99'" >> ~/.bash_aliases
fi

echo "Initialize SPM12 standalone"
echo "Press Quit when SPM12 is up"
sleep 5
sudo /usr/local/spm12_standalone/run_spm12.sh /usr/local/MATLAB/MCR/${mcrver}
#[ -e ~/.matlab ] && sudo chown -R $(whoami):$(whoami) ~/.matlab

exit

