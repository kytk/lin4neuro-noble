#!/bin/bash

#This script is based on the following links;
#https://tug.org/texlive/quickinstall.html
#https://texwiki.texjp.org/?Linux#texliveinstall


cd $HOME/Downloads
[[ -f install-tl-unx.tar.gz ]] && rm install-tl-unx.tar.gz && rm -rf install-tl-*
curl -OL http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar xvf install-tl-unx.tar.gz

cd $(ls | grep install-tl-20)
sudo ./install-tl --no-interaction -repository http://mirror.ctan.org/systems/texlive/tlnet/

version=$(ls /usr/local/texlive/ | grep 20 | sort -r | head -n 1)

echo '' >> ~/.bash_aliases
echo '# PATH settings for TexLive' >> ~/.bash_aliases
echo "MANPATH=\${MANPATH}:/usr/local/texlive/${version}/texmf-dist/doc/man" >> ~/.bash_aliases
echo "INFOPATH=\${INFOPATH}:/usr/local/texlive/${version}/texmf-dist/doc/info" >> ~/.bash_aliases
echo "PATH=\${PATH}:/usr/local/texlive/${version}/bin/x86_64-linux" >> ~/.bash_aliases

# add symbolic links to /usr/local/bin
sudo /usr/local/texlive/${version}/bin/x86_64-linux/tlmgr path add

exit

