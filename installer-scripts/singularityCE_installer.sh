#!/bin/bash
# install script for Singularity CE on Ubuntu
# based on the instruction below
# https://github.com/sylabs/singularity/releases/tag/v3.11.3

set -x

uver=jammy
ver=3.11.3

cd $HOME/Downloads

if [[ ! -e singularity-ce_${ver}-${uver}_amd64.deb ]]; then
  curl -OL https://github.com/sylabs/singularity/releases/download/v${ver}/singularity-ce_${ver}-${uver}_amd64.deb
fi

sudo apt install -y ./singularity-ce_${ver}-${uver}_amd64.deb

exit

