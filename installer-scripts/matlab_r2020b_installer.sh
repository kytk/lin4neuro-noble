#!/bin/bash
#Install Matlab R2020b

zipfile=matlab_R2020b_glnxa64.zip


echo "Install Matlab R2020b (v99)"
cd $HOME/Downloads

if [ ! -e $zipfile ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/${zipfile}
fi

mkdir matlab_R2020b
cd matlab_R2020b
unzip ../${zipfile}

#Install 
sudo ./install

# Change permission of pathdef.m
echo "change permission of pathdef.m so that user can save path"
sudo chmod 666 /usr/local/MATLAB/R2020b/toolbox/local/pathdef.m

echo "Finished!"

exit

