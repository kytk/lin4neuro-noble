#!/bin/bash
# Installer script for AnyDesk
# 06 May 2023 K. Nemoto

# For debugging
set -x

package=anydesk
pubkey=${package}.asc
keylink='https://keys.anydesk.com/repos/DEB-GPG-KEY'
aptlink='http://deb.anydesk.com/ all main'
aptlist=${package}-stable.list

cd $HOME/Downloads
[[ -f ${pubkey} ]] && rm ${pubkey}
curl ${keylink} -o ${pubkey}

sudo gpg --output /usr/share/keyrings/${package}.gpg --dearmor ${package}.asc 

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/${package}.gpg] ${aptlink}" | sudo tee /etc/apt/sources.list.d/${aptlist}

sudo apt-get update
sudo apt-get install -y ${package}
