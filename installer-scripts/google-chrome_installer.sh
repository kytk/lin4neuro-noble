#!/bin/bash

# Script to download and install Google Chrome
# K. Nemoto 05 Mar 2024

cd ~/Downloads
[[ -e google-chrome-stable_current_amd64.deb ]] && \
  rm google-chrome-stable_current_amd64.deb

# Install Google-chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install -y ./google-chrome-stable_current_amd64.deb

