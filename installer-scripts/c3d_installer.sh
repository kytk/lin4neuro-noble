#!/bin/bash

archive='c3d-1.0.0-Linux-x86_64.tar.gz'

echo "Install c3d"
cd $HOME/Downloads

if [ ! -e $archive ]; then
  curl -O http://www.lin4neuro.net/lin4neuro/neuroimaging_software_packages/$archive
fi

cd /usr/local
#remove previous version
sudo tar xvzf ~/Downloads/$archive
sudo mv ${archive%.tar.gz} c3d
sudo chown $USER:$USER /usr/local/c3d

grep c3d ~/.bash_aliases > /dev/null
if [ $? -eq 1 ]; then
    echo '' >> ~/.bash_aliases
    echo '#c3d' >> ~/.bash_aliases
    echo 'export PATH=$PATH:/usr/local/c3d/bin' >> ~/.bash_aliases
fi

echo "Finished!"
sleep 5
exit
