#!/bin/bash

cd $HOME/Downloads
[[ -e fslinstaller.py ]] && rm fslinstaller.py
curl -O https://fsl.fmrib.ox.ac.uk/fsldownloads/fslinstaller.py

fslinstalled=$(which fsl)
if [[ -z "$fslinstalled" ]]; then
  echo "FSL is not installed yet."
  echo "FSL is to be installed with fslinstaller.py"
  echo "checking if fslinstaller.py is downloaded."

  /usr/bin/python3 fslinstaller.py -d /usr/local/fsl

else
  echo "FSL is already installed."
  echo "Do you want to reinstall or update?(yes/no)"
  read answer
  case $answer in 
	[Yy]*)
                /usr/bin/python3 fslinstaller.py -d /usr/local/fsl
		sleep 5
		exit
		;;
	[Nn]*)
		echo -e "FSL will not be updated.\n"
		sleep 5
		exit
  		;;
	*)
		echo -e "Type yes or no.\n"
		;;
  esac

fi

exit

