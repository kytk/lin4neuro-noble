#!/bin/bash
#Script to buid mrtrix3 for Ubuntu 22.04

#Install prerequisite packages
echo "Begin installation of MRtrix3"
echo "Install prerequisite packages"
sudo apt-get install -y git g++ python-is-python3 libeigen3-dev zlib1g-dev libqt5opengl5-dev \
    libqt5svg5-dev libgl1-mesa-dev libfftw3-dev libtiff5-dev libpng-dev

#Download MRtrix3 source
echo "Download MRtrix3 source"
if [ ! -e $HOME/git ]; then
 mkdir $HOME/git
fi

cd $HOME/git
git clone https://github.com/MRtrix3/mrtrix3.git

#Configuration and build
echo "Configure and Build MRtrix3"
cd mrtrix3
./configure
./build
./package_mrtrix

pushd _package
zip -r mrtrix3_jammy.zip mrtrix3
popd

echo 'Finished'

