#!/bin/bash
# build SPM12 standalone for Linux

# For debugging
set -x

ubuntuver=jammy
mcrver=R2022b

[[ -d /standalone ]] || sudo mkdir /standalone
sudo chown $USER:$USER /standalone
cd /standalone
if [[ -d spm12 ]]; then
  pushd spm12
  git pull
  popd
else
  git clone https://github.com/spm/spm12.git
fi
pushd spm12/config
matlab -nodesktop -nosplash -r 'rmpath("~/git/spm12;/usr/local/fsl/etc/matlab;~/Documents/MATLAB;~/matlab");addpath("/standalone/spm12");spm_make_standalone; exit'
popd
mv standalone spm12_standalone
zip -r spm12_standalone_${ubuntuver}_${mcrver}.zip spm12_standalone

