#!/bin/bash
# build CONN standalone for Linux

# For debugging
set -x

ubuntuver=jammy
mcrver=R2022b
connver=22a

[[ -d /standalone ]] || sudo mkdir /standalone
sudo chown $USER:$USER /standalone
cd /standalone
[[ -d conn ]] && rm -rf conn

pushd ~/Downloads
[[ -f conn22a.zip ]] || \
curl http://lin4neuro.net/lin4neuro/neuroimaging_software_packages/conn22a.zip
popd

unzip ~/Downloads/conn22a.zip
pushd conn/standalone
matlab -nodesktop -nosplash -r 'rmpath("~/git/spm12;/usr/local/fsl/etc/matlab;~/Documents/MATLAB;~/matlab");addpath("/standalone/conn;/standalone/spm12");make_linux; exit'
popd

mv conn_standalone/conn22a_glnxa64.zip .

exit

